import React from 'react';
import '../styles/App.css';
import List from './List';
import ListItem from './ListItem';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ListItem>
		  <List />
		</ListItem>
      </header>
    </div>
  );
}

export default App;
