import React, { useState } from 'react';

const users = [
  {
    username: 'Matej Ruzic',
    age: 20,
    regDate: new Date().toDateString(),
  },
  {
    username: 'Luka Ruzic',
    age: 20,
    regDate: new Date().toDateString(),
  },
  {
    username: 'Violeta Antic',
    age: 33,
    regDate: new Date().toDateString(),
  },
  {
    username: 'Jagoda Antic-Ruzic',
    age: 53,
    regDate: new Date().toDateString(),
  },
  {
    username: 'Hrvoje Ruzic',
    age: 49,
    regDate: new Date().toDateString(),
  },
];

const List = () => {
  const [count] = useState(users.length);

  return (
    <>
      <ul>
        {users.map((element) => (
          <li key={element.username}>
            <p>Username: {element.username}</p>
            <p>Age: {element.age}</p>
            <p>Registration date: {element.regDate}</p>
          </li>
        ))}
      </ul>
      <p>{count} users</p>
    </>
  );
};

export default List;
